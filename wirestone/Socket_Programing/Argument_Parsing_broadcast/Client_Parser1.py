import socket
import argparse

client_socket= socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
def receive_message():
    """
    Description.
    receive_message receives the data or message from server on same or different ip address
    """
    # Description of the client_parser1.py
    parser = argparse.ArgumentParser(description = "Receive message from server")
    # parsing arguments at command prompt
    parser.add_argument('ip', help='client ip')
    parser.add_argument('port', help='client port', type=int)
    args=parser.parse_args()
    client_socket.bind((args.ip, args.port))
    try:
        while True:
            data, address = client_socket.recvfrom(1024)
            decoded_data=data.decode('utf-8')
            if decoded_data!="quit":
                print(decoded_data)
            else:
                print("connection closed")
                client_socket.close()
    except Exception as e:
        print(e)

receive_message()