import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {Router} from '@angular/router'
import {CookieService} from 'angular2-cookie/core';
import {HeaderService} from '../../services/header.service';

@Component({
  selector: 'app-userdashboard',
  templateUrl: './userdashboard.component.html',
  styleUrls: ['./userdashboard.component.css']
})
export class UserdashboardComponent implements OnInit {

constructor(private _authenticationService: AuthenticationService,
private router: Router,  private _cookieService:CookieService,
private _headerservice:HeaderService
){}

openNav() {
    document.getElementById("mySidenav").style.width = "250px";

}

closeNav() {
    document.getElementById("mySidenav").style.width = "0";

}



myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}

logout(){
alert(logout);
}


ngOnInit() {
  }

}
