import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import {HeaderService} from './header.service';
import {CookieService} from 'angular2-cookie/core';

@Injectable()
export class AuthenticationService {
    domain = "http://127.0.0.1:5000";
   constructor(private http : Http, private _headerservice: HeaderService, private _cookieService: CookieService) {

   }

newuser(user){
 console.log(this.domain + '/api/1.0/user/add');
 console.log(' i am in newuser');
 return this.http.post(this.domain + '/api/1.0/user/add', user).map(res => res.json());
   }

login(user){
   console.log(this.domain + '/api/1.0/user/login');
   console.log('i am here');
   return this.http.post(this.domain + '/api/1.0/user/login', user).map(res => res.json());
}


logout(){
    var options = this._headerservice.getauthenticationheader(this._cookieService.get('token'));

     //console.log(headerdata);
     console.log(options);
     return this.http.post(this.domain+ '/api/1.0/user/logout',{}, options).map(res => res.json());
}

getinformation(){
    var options = this._headerservice.getauthenticationheader(this._cookieService.get('token'));
    return this.http.get(this.domain+ '/api/1.0/language',options).map(res => res.json());
}

gettopics(){
    var options = this._headerservice.getauthenticationheader(this._cookieService.get('token'));
    return this.http.get(this.domain+ '/api/1.0/topics',options).map(res => res.json());
}

addquestion(data){
    var options = this._headerservice.getauthenticationheader(this._cookieService.get('token'));
    return this.http.post(this.domain+ '/api/1.0/questions',data, options).map(res => res.json());
}

addnewlanguage(data){
    var options = this._headerservice.getauthenticationheader(this._cookieService.get('token'));
    return this.http.post(this.domain+ '/api/1.0/language',data, options).map(res => res.json());
}

addnewtopic(data){
    var options = this._headerservice.getauthenticationheader(this._cookieService.get('token'));
    return this.http.post(this.domain+ '/api/1.0/topic',data, options).map(res => res.json());
}

getquestions(data){
    var options = this._headerservice.getauthenticationheader(this._cookieService.get('token'));
    return this.http.post(this.domain+ '/api/1.0/questions/search', data, options).map(res => res.json());
}

editquestion(data, info){
    var options = this._headerservice.getauthenticationheader(this._cookieService.get('token'));
    return this.http.post(this.domain+ '/api/1.0/question/update/'+data.id ,info, options).map(res => res.json());
}

deletequestion(value){
    var options = this._headerservice.getauthenticationheader(this._cookieService.get('token'));
    return this.http.delete(this.domain+ '/api/1.0/question/'+value.id, options).map(res => res.json());
}

getuserdetails(){
    var options = this._headerservice.getauthenticationheader(this._cookieService.get('token'));
    return this.http.get(this.domain+ '/api/1.0/users', options).map(res => res.json());
}

getlanguagetopics(data){
    var options = this._headerservice.getauthenticationheader(this._cookieService.get('token'));
    return this.http.post(this.domain+ '/api/1.0/language/topics', data, options).map(res => res.json());

}

gettopicquestions(topic_id){
    var options = this._headerservice.getauthenticationheader(this._cookieService.get('token'));
    return this.http.get(this.domain+ '/api/1.0/question/'+ topic_id, options).map(res => res.json());
}

setuserresponse(question, response){
    var options = this._headerservice.getauthenticationheader(this._cookieService.get('token'));
    return this.http.post(this.domain+ '/api/1.0/user/response/'+ question.id, response, options).map(res => res.json());

}

getuserperformance(topic_id){
    var options = this._headerservice.getauthenticationheader(this._cookieService.get('token'));
    return this.http.get(this.domain+ '/api/1.0/user/result/'+ topic_id , options).map(res => res.json());

}

deletetestentry(topic_id){
    var options = this._headerservice.getauthenticationheader(this._cookieService.get('token'));
    return this.http.delete(this.domain+ '/api/1.0/user/delete/entry/'+ topic_id , options).map(res => res.json());
}



}
