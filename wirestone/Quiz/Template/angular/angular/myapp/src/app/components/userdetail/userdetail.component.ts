import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
import {CookieService} from 'angular2-cookie/core';
import {HeaderService} from '../../services/header.service';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'app-userdetail',
  templateUrl: './userdetail.component.html',
  styleUrls: ['./userdetail.component.css']
})
export class UserdetailComponent implements OnInit {
public firstname;
public lastname;
public email;
public mobileno
public created;
public updated;
public value = "abcdd";

constructor(private router:Router,
  private _cookieService:CookieService, private _headerservice: HeaderService,
  private _authenticationService: AuthenticationService) {
  }

getuserdetail(){
this._authenticationService.getuserdetails().subscribe(data => {
      this.firstname = data.user_detail[0].first_name;
        this.lastname = data.user_detail[0].last_name;
        this.email = data.user_detail[0].email_id;
        this.created = data.user_detail[0].created;
        this.updated = data.user_detail[0].updated;
        this.mobileno = data.user_detail[0].mobile_number;
   err => {
    alert("Email Id already exist");
      }

   });}




  ngOnInit() {
this.getuserdetail();
  }

}
