from django.conf.urls import url
from myapp import views
from django.views.generic import TemplateView
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    url(r'^index/', views.index, name='index' ),
    url(r'^index1/([0-9]{3})/([0-9]{2})$', views.index1, name='index1'),
    url(r'^login/', views.login, name='login'),
    url(r'^test/', TemplateView.as_view(template_name="myapp/login.html")),
    url(r'^check/', views.check, name='check'),
    url(r'^register1/', views.register1, name='register1'),


]
