from django.contrib import admin
from .models import sample_table, Item, Category
# Register your models here.

admin.site.register(sample_table)
admin.site.register(Item)
admin.site.register(Category)