class Reverseiteration:

    def __init__(self, iterable_sequence):
        self.iterable_sequence = iterable_sequence
        self.index = len(self.iterable_sequence)-1

    def __iter__(self):

        return self

    def __next__(self):
        """
        Description.
        function returns reverse of a list passed in constructor
        """

        reverse_list_content = []

        try:
            for data in range(len(self.iterable_sequence)):
                value = self.iterable_sequence[self.index]
                reverse_list_content.append(value)
                self.index = self.index - 1
            print(reverse_list_content)

        except StopIteration:
            print(" ")


if __name__ == "__main__":
    reverse_iteration_obj = Reverseiteration([6, 0, 9, 1, 2, 6, 7, 7, 0])
    reverse_iteration_obj.__next__()


