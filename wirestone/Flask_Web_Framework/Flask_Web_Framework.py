from flask import Flask, render_template, redirect, url_for, request, session
import pymysql
import smtplib
import flask_mail
from flask_mail import Mail, Message

MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 465
MAIL_USERNAME = 'ps30121996@gmail.com'
MAIL_PASSWORD = 'helloeveryone'
MAIL_USE_TLS = False
MAIL_USE_SSL = True
app = Flask(__name__, template_folder='Templates')
app.config.from_object(__name__)
app.secret_key = 'luvmathur'
mail = Mail(app)


@app.route('/start')
def start_page():
    return render_template('form.html')


@app.route('/login', methods=['POST'])
def login():
    if request.method == 'POST':
        email_id = request.form['email']
        password = request.form['password']
        mycursor.execute('select Email_Id, Password from sign_up')
        username_login_data = dict(mycursor.fetchall())
        mycursor.execute("select First_Name from sign_up where password="+password+" ")
        first_name = mycursor.fetchone()
        get_id_query = 'select id from sign_up where Email_Id="' + email_id + '"'
        mycursor.execute(get_id_query)
        id = mycursor.fetchone()

        if (email_id, password) in username_login_data.items():
            return render_template('My_Account.html', name=first_name[0] )

        else:
            return redirect(url_for('login_failed'))


@app.route('/logout')
def logout():
    session.pop('User', None)
    return render_template('form.html')


@app.route('/failed')
def login_failed():
    return 'Invalid credentials'


@app.route('/new_user', methods=['POST', 'GET'])
def new_user():
    first_name = request.form['first_name']
    last_name = request.form['last_name']
    email = request.form['email']
    password = request.form['password']
    phone = request.form['phone']
    check_email_query = 'select id from sign_up where Email_Id="'+email+'"'
    mycursor.execute(check_email_query)
    value = mycursor.fetchone()
    if value == None:
        if request.method == 'POST':
            try:
                sql = 'insert into sign_up(First_Name, Last_Name, Email_Id, Password, Mob_No)values("'+first_name+'", "'+last_name+'","'+email+'","'+password+'","'+phone+'")'
                mycursor.execute(sql)
                conn.commit()
                return render_template('My_Account.html', name=first_name)
            except:
                return 'Data Not Added to Database'
    else:
        return 'Email ID Already registered'


@app.route('/forget', methods=['POST', 'GET'])
def forget_password():
    if request.method == 'POST':
        try:
            registered_id = request.form['email_registered']
            mail = Mail(app)
            message = Message('Regarding Change Password', sender='ps30121996@gmail.com', recipients=[registered_id], body="Click Below link to set new Password \n http://localhost:63342/training_assignments/Week1/Templates/change_password.html?_ijt=q9p8efccsgfo41oi97r59tf177")
            mail.send(message)
            return render_template('form.html')
        except:
            return 'Mail Not sent'


@app.route('/update_password', methods=['POST', 'GET'])
def update_password():
    if request.method == 'POST':
        new_password = request.form['new_password']
        confirm_password = request.form['conform_password']
        email = request.form['email']
        if new_password == confirm_password:
            try:
                sql_first_name_query = 'select First_Name from sign_up where Email_Id="'+email+'"'
                sql_update_password = 'update sign_up set password=' + new_password + ' where Email_Id = "'+email + '" '
                mycursor.execute(sql_update_password)
                conn.commit()
                mycursor.execute(sql_first_name_query)
                first_name = mycursor.fetchone()
                mail1 = Mail(app)
                confirmation_message = Message('Password updated', sender='ps30121996@gmail.com', recipients=[email], body="Hello"+first_name[0]+"Your Password has been changed successfully")
                mail1.send(confirmation_message)
                return render_template('My_Account.html', name=first_name[0])
            except:
                return 'Database Not Updated'
        else:
            return 'New Password must be same as Confirm Password'


if __name__ == '__main__':
    conn = pymysql.connect(host="localhost", user="root", passwd="", db="flask_framework", use_unicode=True, charset="utf8")
    mycursor = conn.cursor()
    app.run(debug=True)
