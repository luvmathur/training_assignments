def merge_sort(my_list):
    """
       Description.

       merge_sort function taking sample list on which merge sort is to be performed by evaluating mid value and elements
       less than mid value are assigned in lefthalf and greater are assigned in righthalf
       Parameters
       ----------
       @ param list  my_list
           Description of arg1
           sample list on which sorting has to be done

             Returns
       -------
       sort the lefthalf and righthalf and merge and again perform sorting
       """
    if len(my_list) > 1:
        mid = len(my_list) // 2
        lefthalf = my_list[:mid]
        righthalf = my_list[mid:]

        merge_sort(lefthalf)
        merge_sort(righthalf)

        i = 0
        j = 0
        k = 0
        while i < len(lefthalf) and j < len(righthalf):
            if lefthalf[i] < righthalf[j]:
                my_list[k] = lefthalf[i]
                i = i+1
            else:
                my_list[k] = righthalf[j]
                j = j+1
            k = k+1

        while i < len(lefthalf):
            my_list[k] = lefthalf[i]
            i = i+1
            k = k+1

        while j < len(righthalf):
            my_list[k] = righthalf[j]
            j = j+1
            k = k+1
# output

my_list = [7, 0, 1, 3, 4, 8, 0, 0, 1]
merge_sort(my_list)
# printing sorted list
print(my_list)
