from django.conf.urls import url, include

from . import views, serializer
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.routers import DefaultRouter
from myapp.views import SubscriberViewSet






urlpatterns = [
    url(r'^start/',views.start, name = 'start' ),
    url(r'^get_data/', views.get_data, name='get_data'),
    url(r'^get_sample_data/', views.get_sample_data, name='get_sample_data'),
    url(r'^view_data/', views.view_data, name='view_data'),
    url(r'^get_user_data/', views.UserDetail.as_view(), name='UserDetail'),
    url(r'^genericview/', views.Userdetailview.as_view(), name='UserDetailview'),
    url(r'^list/', views.snippet_list, name='list'),
    url(r'^userview/', views.UserView.as_view()),
    url(r'^userviewdetail/(?P<pk>\d+)/$', views.Userdetailviewgeneric.as_view()),
    url(r'^jobview/', views.UserView.as_view()),
    url(r'^jobupdate/(?P<pk>\d+)/$', views.Userdetailviewgeneric.as_view()),
    url(r'^userbaseview/', views.userviewbase, name='userbaseview'),

]

router = DefaultRouter()
router.register(r'subscribers', views.SubscriberViewSet)

urlpatterns =urlpatterns+router.urls
