from django.db import models

# Create your models here.


class User(models.Model):
    name = models.CharField(max_length=10)
    city = models.CharField(max_length=10)
    age = models.IntegerField()

    def __str__(self):
        return self.name+ "   "+ self.city+"  "+ str(self.age)



class Job(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_job')
    job_location = models.CharField(max_length=15)
    job = models.CharField(max_length=15)

    def __str__(self):
        return self.job_location+"   "+self.job



