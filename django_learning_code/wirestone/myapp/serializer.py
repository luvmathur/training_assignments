from rest_framework import serializers
from myapp.models import User, Job
from rest_framework.response import Response




class UserDetailSerialize(serializers.ModelSerializer):
    #name = serializers.CharField(max_length=10, required=True)
    #city = serializers.CharField(max_length=10, required=True)
    #age = serializers.IntegerField()

    class Meta:
        model = User
        fields = ['id', 'name', 'city', 'age']




class JobSerialize(serializers.ModelSerializer):

    class Meta:
        model = Job
        fields = ['id', 'job_location', 'job', 'user_id']




class UserJobNestedserializer(serializers.ModelSerializer):
    user_job = JobSerialize(many=True)

    class Meta:
        model = User
        fields = ['id', 'name', 'city', 'age', 'user_job']


    def create(self, validated_data):
        job_data = validated_data.pop('user_job')
        new_user = User.objects.create(**validated_data)
        for data in job_data:
            Job.objects.create(user = new_user, **data)
        return new_user


    def update(self, instance, validated_data):
        job_data = validated_data.pop('user_job')
        user_job_data = (instance.user_job).all()
        user_job_data = list(user_job_data)

        instance.name = validated_data.get('name', instance.name)
        instance.city = validated_data.get('city', instance.city)
        instance.age = validated_data.get('age', instance.age)
        instance.save()

        for jobdata in job_data:

            job_data.job_location = validated_data.get('job_location', instance.job_location)
            job_data.job = validated_data.get('job', instance.job_location)
            job_data.save()
        return instance


class UserviewBaseserializer(serializers.BaseSerializer):
    """def to_internal_value(self, data):
    name = data.get('name')
    city = data.get('city')
    age = data.get('age')

    if not name:
        raise  serializers.ValidationError({'name':'name is required'})

    if not city:
        raise  serializers.ValidationError({'city':'city is required'})


    if not age:
        raise  serializers.ValidationError({'age':'age is required'})

    if int(age) >= 30:
        raise serializers.ValidationError({'issue':'age must be less than 30'})

    return {'name': name,
            'city':city,
            'age':age

            }"""

    def to_representation(self, instance):
      return {
          'name':instance.name,
          'city':instance.city,
          'age':instance.age

      }

