from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, JsonResponse, request
from django.template import loader
from rest_framework.decorators import api_view
from django.middleware import csrf
from .models import User, Job
from django.db import connection
from rest_framework.views import  APIView
from myapp.serializer import JobSerialize, UserDetailSerialize, UserJobNestedserializer, UserviewBaseserializer
from rest_framework.response import Response
from rest_framework import mixins, generics
from django.views import generic
from rest_framework import permissions, viewsets
from rest_framework.viewsets import ModelViewSet
from rest_framework import status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.utils.six import BytesIO

def start(request):
    template = loader.get_template('view.html')
    array = {}
    return HttpResponse(template.render(array, request))


def get_data(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        city = request.POST.get('city')
        age = request.POST.get('age')
        job_location = request.POST.get('job_location')
        job = request.POST.get('job')
        user_data = User(name = name, city = city, age = age)
        user_data.save()
        job_data = Job( job_location = job_location, job = job)
        job_data.save()
    return HttpResponse("name= "+name+"   "+"city = "+city+"   "+"age =" + age)

def get_sample_data(request):
    cursor = connection.cursor()
    user_id = cursor.execute('select user_id from myapp_job ')
    user = cursor.execute('select name from myapp_user where id = "'+str(user_id)+'" ')
    return HttpResponse(user_id)


def view_data(request):
    template = loader.get_template('data.html')
    user_data = User.objects.all()
    user_job_data = Job.objects.all()

    array = {'data': user_data, 'user_job_data': user_job_data}
    return HttpResponse(template.render(array, request))


# Create your views here.



class UserDetail(APIView):
    #authentication_classes = (SessionAuthentication, BasicAuthentication)
    #permission_classes = (permissions.IsAuthenticated, )

    allowed_methods = ['GET', 'POST', 'PUT']

    def post(self, request):
        """if request.method == 'POST':
            name = request.POST.get('name')
            city = request.POST.get('city')
            age =  request.POST.get('age')
            user_data = User(name=name, city = city,  age = age)
            user_data.save()
            job_location = request.POST.get('job_location')
            job = request.POST.get('job')
            job_data = Job(job_location = job_location, job = job)
            job_data.save()
            return Response("data added successfully")
        """
        serializer = UserDetailSerialize(data = request.data)
        permission_classes = (permissions.IsAuthenticatedOrReadOnly)
        if serializer.is_valid():
            serializer.save()
        job_data = Job(job_location = "agra", job = "dev")
        job_data.save()

        return Response(User.objects.all())


    def get(self, request):
            user_detail = User.objects.all()
            serializer = JobSerialize(user_detail, many=True)
            return Response(serializer.data)



def snippet_list(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        snippets = User.objects.all()
        serializer = UserDetailSerialize(snippets, many=True)
        return JsonResponse(serializer.data, safe=False)



class  SubscriberViewSet(viewsets.ModelViewSet, APIView):
    serializer_class = UserDetailSerialize
    queryset = User.objects.all()


    def list(self, request):
        queryset = User.objects.all()
        serializer = UserDetailSerialize(queryset, many=True)
        return Response(serializer.data)



    def retrieve(self, request,pk=None):
        query = User.objects.get(pk=pk)
        serializer = UserDetailSerialize(query)
        return Response(serializer.data)

    def partial_update(self, request, pk =None):
       # user = User.objects.get(pk=pk)
        user_update = User(data = request.data)
        serializer = UserDetailSerialize(user_update)
        serializer.save()
        return Response("data updated")



    def destroy(self, request,pk = None):
        user = User.objects.get(pk = pk)
        user.delete()
        return Response("user deleted")


    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
        job_data = Job(job_location="agra", job="dev")
        job_data.save()

        new_entry = User(serializer, age = '22')
        return Response(serializer.data, status= status.HTTP_201_CREATED)


class Userdetailview(generics.ListCreateAPIView):

    serializer_class = UserDetailSerialize
    allowed_methods = ['GET','POST',]
    def get_queryset(self):
        query = User.objects.all()
        return query


    def post(self, request):
        data = UserDetailSerialize(data = request.data)
        if data.is_valid():
            data.save()
        job_data = Job(job_location="agra", job="dev")
        job_data.save()
        return User.objects.all()



class UserView(generics.ListCreateAPIView):
    serializer_class = UserJobNestedserializer
    queryset = User.objects.all()


class Userdetailviewgeneric(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = UserJobNestedserializer
    queryset = User.objects.all()



class Jobview(generics.ListCreateAPIView):
    serializer_class = JobSerialize
    queryset = Job.objects.all()

class Jobviewupdate(generics.UpdateAPIView):
    serializer_class = JobSerialize
    queryset = Job.objects.all()


@api_view(['GET',])
def userviewbase(request):
    user = User.objects.all()
    serializer = UserviewBaseserializer(user)
    return Response(serializer.data)
