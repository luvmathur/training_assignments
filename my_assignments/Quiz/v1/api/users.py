from Quiz.config import db
from flask import Flask, jsonify, request, json

import datetime
import jwt
from werkzeug.security import generate_password_hash, check_password_hash
from functools import wraps
from flask import Blueprint

user_api = Blueprint('user_api', __name__)

current_date_and_time = str(datetime.datetime.now())


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        if 'access-token' in request.headers:
            token = request.headers['access-token']

        if not token:
            return jsonify({'Warning': 'Token Missing', 'status': '499'}), 499
        try:
            data = jwt.decode(token, '')
            current_user = data['id']
        except:
            return jsonify({'Message': 'Token is invalid', 'status': '498'}), 498

        return f(current_user, *args, **kwargs)
    return decorated


@user_api.route('/api/1.0/users', methods=['GET'])
@token_required
def get_all_users(current_user):
    """
    Description.
    get_all_users returns all the users registered

    """

    try:
            db.mycursor.execute('select id, first_name, last_name , email_id, mobile_number, created, updated from users')
            user_info = db.mycursor.fetchall()
            info = []
            for data in range(len(user_info)):
                info.append({'id': user_info[data][0], 'first_name': user_info[data][1], 'last_name': user_info[data][2], 'email_id': user_info[data][3], 'mobile_number': user_info[data][4], 'created': user_info[data][5], 'updated': user_info[data][6]})
            db.mycursor.execute('select status from tokens where user_id ="'+str(current_user)+'"')
            status = db.mycursor.fetchone()
            if status[0] == 'Active':
                return jsonify({'user detail': info}), 200
            else:
                return jsonify({'Message': 'permission denied'})

    except Exception:
        return jsonify({'error': {'message': 'Token expire', 'status': '440'}}), 440


@user_api.route('/api/1.0/user/add', methods=['POST', 'GET'])
def user_add():
    """
        Description.
        user_add returns register new user
    """

    if request.method == 'POST':
        try:
            user_informarion = request.get_json()
            first_name = user_informarion['first_name']
            last_name = user_informarion['last_name']
            email = user_informarion['email']
            password = user_informarion['password']
            mobile_number = user_informarion['mobile_number']
            hashed_password = generate_password_hash(password)
            db.mycursor.execute(' select id from users where email_id = "'+email+'" ')
            user_id = db.mycursor.fetchone()
            if user_id is None:
                db.mycursor.execute('insert into users(first_name, last_name, email_id, password, mobile_number,role_id) values("'+first_name+'","'+last_name+'","'+email+'","'+hashed_password+'","'+mobile_number+'","1")')
                db.conn.commit()
                db.mycursor.execute('update users set created = "' + str(current_date_and_time) + '" where email_id = "' + email + '"')
                db.conn.commit()
                return jsonify({'message': 'User added successfully', 'status': '201'}), 201

            else:
                return jsonify({'error': {'Message': 'Email Id already exist', 'status': '409'}}), 409
        except Exception as e:
            return e

    else:
        return jsonify({'error': {'Message': 'HTTP request not supported', 'status': '405'}}), 405


@user_api.route('/api/1.0/user/login', methods=['POST', 'GET', 'PUT'])
def login():
    """
        Description.
        login returns a unique token on entering correct credentials for every registered user

    """

    if request.method == 'POST':
        email = request.json['email']
        password = request.json['password']
        hash_password = generate_password_hash(password)
        db.mycursor.execute('select first_name from users where email_id = "'+email+'"')
        first_name = db.mycursor.fetchone()
        db.mycursor.execute('select id from users where email_id = "'+email+'"')
        user_id = db.mycursor.fetchone()
        db.mycursor.execute('select password from users where email_id = "'+email+'"')
        user_password = db.mycursor.fetchone()
        if first_name is None:
            return jsonify({'error': {'status': '401', 'message': 'UnAuthorized'}}), 401
        if check_password_hash(user_password[0], password) == True:
            token = jwt.encode({'id': user_id[0], 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=30)}, "")
            db.mycursor.execute('insert into tokens(user_id, token, status)values("'+str(user_id[0])+'", "'+str(token)+'", "Active")')
            db.conn.commit()
            return jsonify({'message': {'Logged In': {'name': first_name[0], 'status': '200', 'token': token.decode('UTF-8')}}}), 200
        else:

            return jsonify({'error': {'status': '401', 'message': 'UnAuthorized'}}), 401
    else:
        return jsonify({'error': {'Message': 'HTTP request not supported'}}), 405


@user_api.route('/api/1.0/user', methods=['DELETE'])
@token_required
def delete_user(current_user):
    """
        Description.
        delete_user deletes the current user account
    """

    db.mycursor.execute('select id from users where id="'+str(current_user)+'"')
    user_id = db.mycursor.fetchone()
    db.mycursor.execute('select status from tokens where user_id ="' + str(current_user) + '"')
    status = db.mycursor.fetchone()

    if user_id is None:
        return jsonify({'error': {'Message': 'user id do not exist', 'status': '404'}})
    else:
        if status is not None:
            db.mycursor.execute('delete from tokens where user_id = "' + str(current_user) + '"')
            db.conn.commit()
            db.mycursor.execute('delete from users where id = "' + str(current_user) + '" ')
            db.conn.commit()
            return jsonify({'Message': 'Requested user id deleted', 'status': '200'}), 200
        else:
            return jsonify({'error': {'Message': 'Token Expire', 'status': '440'}}), 440


@user_api.route('/api/1.0/user/update', methods=['POST'])
@token_required
def update_user(current_user):
    """
        Description.
        update_user updates  the current user account details
        """

    first_name = request.json['first_name']
    last_name = request.json['last_name']
    email = request.json['email']
    password = request.json['password']
    mobile_number = request.json['mobile_number']
    hash_password = generate_password_hash(password)
    db.mycursor.execute('select id from users where id="' + str(current_user) + '"')
    user_id = db.mycursor.fetchone()
    db.mycursor.execute('select count(email_id)from users where email_id ="' + email + '"')
    count = db.mycursor.fetchone()
    db.mycursor.execute('select status from tokens where user_id ="' + str(current_user) + '"')
    status = db.mycursor.fetchone()
    if user_id is None:
        return jsonify({'Message': 'user id do not exist'}), 404

    else:
        if status is not None:
            if str(count[0]) == str(0):
                db.mycursor.execute('update users SET first_name = "'+first_name+'", last_name = "'+last_name+'", email_id = "'+email+'",password = "'+hash_password+'", mobile_number ="'+mobile_number+'", updated ="'+current_date_and_time+'" where id="'+str(current_user)+'"')
                db.conn.commit()
                return jsonify({'status': 'Record updated successfully'}), 200
            else:
                db.mycursor.execute('update users SET first_name = "' + first_name + '", last_name = "' + last_name + '", email_id = "' + email + '",password = "' + hash_password + '", mobile_number ="' + mobile_number + '", updated ="' + current_date_and_time + '" where id="' + str(current_user) + '"')
                db.conn.commit()
                return jsonify({'Message': 'your previous and updated Email Id is same'}), 200
        else:
            return jsonify({'Message': 'Token Expire'}), 498


@user_api.route('/api/1.0/user/forget_password/<id>', methods=['POST'])
def forget_password(id):
    """
        Description.
        forget_password regenerate a new password to a current_user
    """

    new_password = request.json['new_password']
    confirm_password = request.json['confirm_password']
    db.mycursor.execute('select id from users where id="' + id + '"')
    user_id = db.mycursor.fetchone()
    if user_id is None:
        return jsonify({'Message': 'user id does not exist'})
    else:
        if new_password == confirm_password:
            hash_password = generate_password_hash(new_password)
            db.mycursor.execute('update users set password = "' + hash_password + '", updated ="' + current_date_and_time + '" where id ="' + id + '"')
            db.conn.commit()
            return jsonify({'status': 'Password changed successfully'}), 200
        else:
            return jsonify({'status': 'new password and confirm password should be same'}), 400


@user_api.route('/api/1.0/user/logout', methods=['POST'])
@token_required
def logout(current_user):
    """
        Description.
        logout expires the current active token of a user who is logged in
    """

    if current_user:
        db.mycursor.execute('delete from tokens where user_id = "'+str(current_user)+'"')
        db.conn.commit()
        return jsonify({'message': 'Logged out', 'status': '200'}), 200


