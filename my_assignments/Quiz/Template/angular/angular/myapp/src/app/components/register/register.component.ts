import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../services/authentication.service';
import {Router} from '@angular/router'
import {CookieService} from 'angular2-cookie/core';
import {HeaderService} from '../../services/header.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [ CookieService ]
})


export class RegisterComponent implements OnInit {


constructor(private authenticationService: AuthenticationService,
private router: Router,  private _cookieService:CookieService,
private _headerservice:HeaderService
){}


onSubmit(value:any){
console.log(value);
var self = this ;
var getheadertoken;
this.authenticationService.login(value).subscribe(data => {
      this._cookieService.put('token', data.message.logged_in.token);
      this._headerservice.setauthenticationheader(data.message.logged_in.token);
      getheadertoken = this._headerservice.getauthenticationheader(data.message.logged_in.token);
      console.log(JSON.stringify(getheadertoken));
      console.log(data.message.logged_in.token);
      console.log(value.email, value.password);
      if(value.email=="luvmathur123@gmail.com" && value.password =="123"){
      self.router.navigate(['/home']);
      }
      else{
      self.router.navigate(['/userdashboard']);

      }

err => {
      alert('Invalid credentials');
}
});
}

registerUser(value : any){
  var self = this ;
   console.log(value);
   this.authenticationService.newuser(value).subscribe(data => {
      self.router.navigate(['/register']);
      console.log(data);

   err => {
    alert("Email Id already exist");
      }

   });
}




  ngOnInit() {
  }

}
