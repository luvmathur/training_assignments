def fibonacci_series():
    """
               Description.
               This function generates sum of preceding two values

               @parameter
               no parameter is passed

               Returns
               -------
               store sum of preceding values in a memory

    """

    val1 = 0
    val2 = 1
    boundary = int(input("Enter number of times fibonacci series to continue\n"))
    print(val1, end='    ')
    for count in range(boundary):
        yield val1 + val2
        val1, val2 = val1 + val2, val1

if __name__ == "__main__":
    result = fibonacci_series()
    for val in result:
        print(val, end='      ')
