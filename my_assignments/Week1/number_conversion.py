def deci_to_oct(num):
    """
    Description.
    deci_to_oct conversion  function determines the quotient when given input is divided by 8
    Parameters
    ----------
    @ param int  num
    Description of num
    sample value whose octal value is to be determined

     Returns
     reversed value of actual output
     -------
     value
     """
    number = num
    rem = 0
    while num % 8 != 0:
        rem = 10 * rem + (num % 8)
        num = num // 8
    return rem

deci_to_oct(20)
val = deci_to_oct(20)


rev_num = 0


def reverse_digits():
    """
    Description.
    reverse_digits reverses the output of dec_to_oct function
    Parameters
    ----------
    no parameter

    Returns
    actual octal value by reversing deci_to_oct output
   -------
                      """


a = 0

while val > 0:
    a = val % 10
    rev_num = 10 * rev_num + a
    val = val // 10
print("Octal conversion : %d " % (rev_num))

reverse_digits()



def oct_to_deci(num):
    """
     Description.
     determine the decimal value of given sample octal number
     Parameters
     ----------
     @ param int num
     sample input type int num
     """


    decimal_value = 0
    rem = 0
    a = 0
    number = num
    while num != 0:
        rem = num % 10
        decimal_value = decimal_value + rem * pow(8, a)
        num = num // 10
        a = a+1

    print("decimal conversion %d" % (decimal_value))
oct_to_deci(20)
