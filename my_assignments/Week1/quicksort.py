
def partition(my_list, first, last):
    """
    Description.

    partition function taking my_list ,its first and last value as argument and partition of my_list is performed

    Parameters
    ----------
    @ param list  my_list
        Description of arg1
        sample list on which sorting has to be done
    @ param int  first
         first value of list

    @ param int last
         last value of list

    Returns
    -------
    sample list is partitioned in two parts
    """
    pointer_index = (first - 1)
    pivot = my_list[last]

    for index in range(first, last):
        if my_list[index] <= pivot:
            pointer_index = pointer_index + 1
            my_list[pointer_index], my_list[index] = my_list[index], my_list[pointer_index]

    my_list[pointer_index + 1], my_list[last] = my_list[last], my_list[pointer_index + 1]
    return (pointer_index + 1)


def quickSort(my_list, first, last):
    """
        Description.

        quicksort function taking sample list ,its first and last value as argument.

        Parameters
        ----------
        @ param list  my_list
            Description of arg1
            sample list on which sorting has to be done
        @ param int  first
             first value of list

        @ param int  last
             last value of list

        Returns
        -------
        elements less than pivot are before pivot and greater are after pivot
        """
    if first < last:
        pi = partition(my_list, first, last)

        quickSort(my_list, first, pi - 1)
        quickSort(my_list, pi + 1, last)

# Output

my_list = [10, 7, 8, 9, 1, 5]
n = len(my_list)
quickSort(my_list, 0, n-1)
print("Sorted array is:")
for sorted_list_index in range(n):
    # printing sorted array
    print("%d\t" % my_list[sorted_list_index])