from django.db import models
from django import forms

# Create your models here.


class sample_table(models.Model):
    name = models.CharField(max_length=10)
    designation = models.CharField(max_length=10)

    def __str__(self):
        return "{0} vs {1}".format(self.name, self.designation)




class Register(models.Model):
    gender = (
                   ('M', 'Male'), ('F', 'Female')
              )
    name = models.CharField(max_length=10)
    gender =models.CharField(max_length=1, choices= gender)

    def __str__(self):
        return "{0} vs {1}".format(self.name, self.gender)




class Category(models.Model):
    name = models.CharField(max_length=10)

    def __str__(self):
        return "{0} vs {1}".format(self.name)


class Item(models.Model):
        name = models.CharField(max_length=20)
        category = models.ForeignKey(Category)
        def __str__(self):
            return "{0} vs {1}".format(self.name, self.category)